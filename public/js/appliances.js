// ----------------------- class toggling for filters
$('#filter-container').on('hidden.bs.collapse', function(){
    $('#filter-toggle').find('i').removeClass('fa-caret-up');
    $('#filter-toggle').find('i').addClass('fa-caret-down');
});

$('#filter-container').on('shown.bs.collapse', function(){
    $('#filter-toggle').find('i').removeClass('fa-caret-down');
    $('#filter-toggle').find('i').addClass('fa-caret-up');
});
// ---------------------------------------------------

/**
 * Update current url with a value of get parameter. 
 */
var replaceUrlParameter = function(param, value)
{
    var url = document.location.href;    
    var regex = new RegExp('([?&]' + param + '=)[\\da-zA-Z]+');
    // check is the param parameter already set in the url
    if (regex.test(url)) {
        // if parameter set, just replace the value
        newUrl = url.replace(regex, "$1" + value);
        document.location.href = newUrl;
    } else {
        // if parameter not set, add it to the url
        if (url.indexOf('?') > -1) {
            url += '&';
        } else {
            url += '?';
        }
        document.location.href = url + param + '=' + value;
    }
};

// go back to the top of the page 
var toTop = function()
{
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}

// update page sorting and reload page
$('#sort-by').on('change', function(){
    replaceUrlParameter('order_by', $(this).val());
});

// update number of product on the page and reload
$('#how-many').on('change', function(){
    replaceUrlParameter('how_many', $(this).val());
});

// sub-page navigation (previous, next, last, first page)
$('#page-select .btn-primary').on('click', function(){
    var page = parseInt($('#page-select').attr('data-page'));
    var pageNumber = parseInt($('#page-select').attr('data-page-number'));
    switch ($(this).attr('id')) {
        case "btn-top":
            toTop();
            break;

        case "btn-next":
            replaceUrlParameter('page', page + 1);
            break;
        
        case "btn-prev":
            replaceUrlParameter('page', page - 1);
            break;
        
        case "btn-last":
            replaceUrlParameter('page', pageNumber);
            break;
        
        case "btn-first":
            replaceUrlParameter('page', 1);
            break;
    }
});

/** handled for "Like / Add to wishlist" button. 
* Sends an ajax request and updated button html to reflect it
*/
$(document).on('click', '.btn-like', function(){
    var btn = $(this);
    $(this).attr('disabled', 'disabled');
    var id = $(this).attr('data-product');
    $.get('/like',{'id':id}, function(data){
        if (data.status) {
            btn.removeClass('btn-like');
            btn.addClass('btn-wishlist');
            btn.html('<i class="fas fa-star"></i>&nbsp;On my wishlist');
        } else {
            alert('Sorry! something went wrong');
        }
        btn.removeAttr('disabled');
    });
});

/** 
 * Handler for "Unlike / Remove from wishlist" button. 
 * Sends an ajax request and updated button html to reflect it
*/
$(document).on('click', '.btn-wishlist', function(){
    var btn = $(this);
    $(this).attr('disabled', 'disabled');
    var id = $(this).attr('data-product');
    $.get('/unlike',{'id':id}, function(data){
        if (data.status) {
            btn.removeClass('btn-wishlist');
            btn.addClass('btn-like');
            btn.html('<i class="far fa-star"></i>&nbsp;I like it!');
        } else {
            alert('Sorry! something went wrong');
        }
        btn.removeAttr('disabled');
    });

    var url = document.location.href;
    if (url.search('wishlist') > 0) {
        var container = btn.closest('.product-container');
        container.hide('slow', function(){
            container.before('<div class="row col-12 undo" id="undo-' + id 
                + '"><a href="javascript:undoProductRemove(' 
                + id +')">Undo?</a></div>');
            container.remove(); 
        });
    }
});

/**
 * Handler for "Undo" button (to revert removing a product from wishlist page)
 * Sends two ajax requests:
 * 1) like this product
 * 2) fetch product display html
 */
var undoInProgress = false
var undoProductRemove = function(id)
{
    if (undoInProgress) {
        return;
    }
    undoInProgress = true;
    $.get('/like',{'id':id}, function(data){
        if (data.status) {
            $.get('/product/' + id, function(productHtml){
                element = $('#undo-' + id);
                element.hide('slow', function(){
                    element.before(productHtml);
                    element.remove();
                });
                undoInProgress = false;
            });
        } else {
            undoInProgress = false;
        }
    });
};

var typingTimer;
var sendSearchRequest = function()
{
    var searchText = $('#friend-search').val();
    $.post('/friends/search',{search:searchText}, function(data){
        if (data.status) {
            $('#friend-search-results ul').html(data.html);
        }
    });
};
$(document).on('keyup keydown change', '#friend-search', function(){
    clearTimeout(typingTimer);
    typingTimer = setTimeout(sendSearchRequest, 2000);
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var requestingFriend = false;
$(document).on('click', '.request-friend', function(){
    if (requestingFriend) {
        return;
    }
    requestingFriend = true;
    id = parseInt($(this).attr('data-id'));
    $.get('/friends/add/' + id, function(data){
        $requestingFriend = false;
        location.reload();
    });
});

$('#copy-wishlist-link').on('click', function(){
    var link = $('#wishlist-link-anchor').text().trim();
    var element = document.createElement('textarea');
    element.value = link;
    document.body.appendChild(element);
    element.select();
    document.execCommand('copy');
    document.body.removeChild(element);
    alert('Link copied!');
});

$('.copy-product-link').on('click', function(){
    var link = $(this).closest('.product-container').find('.product-title a').attr('href').trim();
    var element = document.createElement('textarea');
    element.value = link;
    document.body.appendChild(element);
    element.select();
    document.execCommand('copy');
    document.body.removeChild(element);
    alert('Link copied!');
});

$('#share-wishlist-link').on('click', function(){
    var users = $('#share-with-users').val();
    var type = 'wishlist';
    var link = $('#wishlist-link-anchor').text().trim();
    $.post('/share/link',{users:users,type:type,link:link}, function(data){
        if (data.status) {
            alert('Wishlist shared!');
        }
    });
});

$('.share-product-link').on('click', function(){
    var users = $(this).closest('.row').find('select').val();
    var type = 'product';
    var link = $(this).closest('.product-container').find('.product-title a').attr('href').trim();
    console.log(users, type, link);
    $.post('/share/link',{users:users,type:type,link:link}, function(data){
        if (data.status) {
            alert('Product shared!');
        }
    });
});

$('.shared-link-visit').on('click', function(){
    var id = $(this).attr('data-id');
    $.get('/shares/seen/' + id);
});