# My Favourite Appliances

## 1. (optional) Environment setup
To set up a vagrant box for Laravel (and this awesome website) follow steps on [the official Laravel Homestead page](https://laravel.com/docs/5.7/homestead#installation-and-setup)

## 2. Repository setup
###	2.1 Create a directory for this repository
### 2.2 Checkout the code: 
```
git clone https://bitbucket.org/MarcinTraczyk/my-favourite-appliances.git .
```
###	2.3 Run composer install:
```
composer install
```
###	2.4 Create the configuration file based on `.env.examle`

## 3. Setup the database
### 3.1 Run database migrations
```
php artisan migrate --seed
```
### 3.2 Run the crawling script to populate the database
```
php artisan crawler:crawl
```
### 3.3 (Optional) add the above crawler command to crontab to run daily and keep your database up to date

## 4. The website should be ready to view
If you have seeded the database (step 3.1) you should be able to log in as __admin@here.com / password__ ; 
Admin's wishlist is empty but he (she?) starts with a friend, friend invites (one sent out, one received) and few shared links to check out which should help you preview different aspects of the site. 