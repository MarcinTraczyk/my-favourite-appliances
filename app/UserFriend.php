<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserFriend extends Model
{
    protected $fillable = ['user_id', 'friend_id', 'status'];
    
    public function friendUser()
    {
        return $this->hasOne(\App\User::class, 'id', 'friend_id');
    }

    public function user()
    {
        return $this->hasOne(\App\User::class, 'id', 'user_id');
    }

    public static function userIsFriend($id)
    {
        $user = \Auth::user();
        if (!empty($user)) {
            $list = $user
                ->getFriendlist()
                ->toArray();
            return in_array($id, array_keys($list));
        }
        
        return false;
    }

    public function sentOn()
    {
        $timestamp = strtotime($this->created_at);
        return date('d/m/Y');
    }
}
