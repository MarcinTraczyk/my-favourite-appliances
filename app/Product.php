<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Storage;

class Product extends Model
{
    protected $fillable = ['id', 'name', 'price', 'rrp', 'category', 'image_url'];

    /**
     * Re-populates the database table from the input array
     */
    public static function populateTableFromArray($data)
    {
        $extraAttriutes = [];
        $images = [];

        foreach ($data as $productId => &$row) {
            foreach ($row as $key => $value) {
                if (!in_array($key, ['id', 'name', 'price', 'rrp', 'category', 'image_url'])) {
                    // check for attributes array to be saved as separate records
                    if ($key == 'attributes' && is_array($value) && count($value) > 0) {
                        foreach ($value as $attribute) {
                            $extraAttriutes[] = [
                                'product_id'    => $productId,
                                'description'   => $attribute,
                            ];
                        }
                    }
                    unset($row[$key]);
                } else if ($key == 'price') {
                    $value = preg_replace("/[^0-9\.]/", "", $value);
                    $row[$key] = (float)$value;
                } else if ($key == 'rrp' && !is_null($value)) {
                    $value = preg_replace("/[^0-9\.]/", "", $value);
                    $row[$key] = (float)$value;
                } else if ($key == 'image_url') {
                    try {
                        $contents = file_get_contents($value);
                        $newName = md5($productId . $value);
                        Storage::disk('img')->put($newName, $contents);
                    } catch (\Exception $e) {
                        echo "Error downloading image" . PHP_EOL;
                    }
                }
            }
        }
        // wrap up in a transaction to rollback to a current state on error
        \DB::beginTransaction();
        try {
            // \DB::transaction(function() use ($data) {
            Product::truncate();
            ProductAttribute::truncate();
            Product::insert($data);
            ProductAttribute::insert($extraAttriutes);
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            return false;
        }

        // TODO: clean all images no longer needed
        return true;
    }

    /**
     * Search for local image, if not found, fallback to an external url
     */
    public function getImageUrl()
    {
        $url = '';
        if (!empty($this->image_url)) {
            $newName = md5($this->id . $this->image_url);
            if (file_exists(public_path() . '/img/' . $newName)) {
                $url = '/img/' . $newName;
            } else {
                $url = $this->image_url;
            }
        }

        return $url;
    }

    public static function getPriceRanges()
    {
        return [
            [
                "display"   => "< 10€",
                "min"       => null,
                "max"       => "10",
            ],
            [
                "display"   => "10 - 20 €",
                "min"       => "10",
                "max"       => "20",
            ],
            [
                "display"   => "20 - 50 €",
                "min"       => "20",
                "max"       => "50",
            ],
            [
                "display"   => "50 - 100 €",
                "min"       => "50",
                "max"       => "100",
            ],
            [
                "display"   => "50 - 100 €",
                "min"       => "50",
                "max"       => "100",
            ],
            [
                "display"   => "100 - 500 €",
                "min"       => "100",
                "max"       => "500",
            ],
            [
                "display"   => "> 500 €",
                "min"       => "500",
                "max"       => null,
            ],
        ];
    }

    public function attributes()
    {
        return $this->hasMany('App\ProductAttribute');
    }

    public function wishlist()
    {
        return $this->hasMany('App\UserWishlist');
    }

    public function getDiscountPercentage()
    {
        if (!($this->rrp > 0)) {
            return false;
        }
        $val = 100.0 * ($this->rrp - $this->price) / ($this->price);
        return round($val,1) . "%";
    }

    /**
     * check is this product on the wishlist of the user currently logged in
     */
    public function onWishlist()
    {
        return UserWishlist::productOnWishlist($this->id);
    }
}
