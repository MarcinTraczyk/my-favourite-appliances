<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\DataProvider\Crawler as CrawlerClass;
use App\Product;
use Illuminate\Filesystem\Filesystem;

class Crawler extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Crawler:crawl';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crawl appliancesdelivered.ie to populate local database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $endpoints = [
            [
                'url'       => "https://www.appliancesdelivered.ie/dishwashers",
                'batch'     => 3,
                'category'  => 'Dishwasher',
            ],
            [
                'url'       => "https://www.appliancesdelivered.ie/small-appliances",
                'batch'     => 21,
                'category'  => 'Small Appliance',
            ],
        ];

        $data = [];
        $file = new Filesystem;
        $test = $file->cleanDirectory('public/img');
        foreach ($endpoints as $point) {
            $c = new CrawlerClass($point['url'], $point['category'], $point['batch']);
            $c->fetchAllPages();
            $res = $c->getResults();
            echo 'Found: ' . count($res['data']) 
                . ' products in "' . $point['category'] . '" category' . PHP_EOL;
            $data += $res['data'];
        }
        $status = Product::populateTableFromArray($data);
        $statusStr = $status ? 'ok' : 'fail';
        echo "Database update status: $statusStr" . PHP_EOL;
    }
}
