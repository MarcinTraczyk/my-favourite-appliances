<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserShare extends Model
{
    protected $fillable = ['sender_id', 'recipient_id', 'type', 'link', 'seen'];

    public function sender()
    {
        return $this->hasOne(\App\User::class, 'id', 'sender_id');
    }
}