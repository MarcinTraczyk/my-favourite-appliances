<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserWishlist;

class Wishlist extends Controller
{
    /**
     * Add prduct to wishlist
     *
     */
    public function add(Request $request)
    {
        try {
            $user       = $request->user();
            $productId  = intval($request->input('id'));
            $status     = 0;
            $message    = 'Fail';
            $exists     = UserWishlist::productOnWishlist($productId);
            if ($exists) {
                $message = 'Item already on wishlist';
            } else if ($user && $productId) {
                $record = new UserWishlist([
                    'product_id'    => $productId,
                    'user_id'       => $user->id,
                ]);
                
                $status = $record->save();
                if ($status) {
                    $message = 'Success';
                } else {
                    $message = 'Error saving record';
                }
            }

        } catch (\Exception $e) {
            $message = 'EPR';
        }

        return response()->json([
            'status'    => $status,
            'message'   => $message,
        ]);
    }

    /**
     * Remove product from the wishlist
     */
    public function remove(Request $request)
    {
        try {
            $user       = $request->user();
            $productId  = intval($request->input('id'));
            $status     = 0;
            $message    = 'Fail';
            $exists     = UserWishlist::productOnWishlist($productId);
            if (!$exists) {
                $message = 'Item not on wishlist';
            } else if ($user && $productId) {
                $record = UserWishlist::where([
                    'product_id' => $productId,
                    'user_id'    => $user->id,
                ])->take(1)->first();
                
                $status = $record->delete();
                if ($status) {
                    $message = 'Success';
                } else {
                    $message = 'Error saving record';
                }
            }

        } catch (\Exception $e) {
            $message = 'EPR';
        }

        return response()->json([
            'status'    => $status,
            'message'   => $message,
        ]);
    }
}
