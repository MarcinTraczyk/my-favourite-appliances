<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class Search extends Controller
{
    protected $isWishlist = false;
    protected $onUserWishlist = null;

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        return $this->addSearch($request, 'Products');
    }

    public function wishlist(Request $request)
    {
        $this->isWishlist = true;
        return $this->addSearch($request, 'Your wishlist', false);
    }

    public function share(Request $request, $hash)
    {
        $user = \App\User::where('wishlist_hash', '=', $hash)->first();
        $header = '';
        if (empty($user)) {
            return redirect('noresource');
        } else {
            $this->onUserWishlist = $user->id;
            $header = $user->name . "'s wishlist";
        }
        return $this->addSearch($request, $header, false);
    }

    protected function addSearch(Request $request, $headerText = '', $showFilters = true)
    {
        $orderValue = $request->input('order_by');
        switch ($orderValue) {
            case 0:
            default:
                $field = 'name';
                $order = 'asc';
                break;
            case 1:
                $field = 'name';
                $order = 'desc';
                break;
            case 2:
                $field = 'price';
                $order = 'desc';
                break;
            case 3:
                $field = 'price';
                $order = 'asc';
                break;
        }
        $productsQuery = Product::orderBy($field, $order);
        
        $productsPerPage = $request->input('how_many');
        // products per page should be a specific value, otherwise a default 10
        if (empty($productsPerPage) || !in_array($productsPerPage, [10, 20, 50])) {
            $productsPerPage = 10;
        }

        $page = intval($request->input('page'));
        // products per page should be a specific value, otherwise a default 10
        if (empty($page) || $page <= 1) {
            $page = 1;
        }
        $offset = ($page - 1) * $productsPerPage;

        // category should be an exact match
        if (!empty($request->input('category'))) {
            $productsQuery->where('category', $request->input('category'));
        }

        // products containing search phrase in the name
        if (!empty($request->input('find'))) {
            $productsQuery->where('name', 'like', '%' . $request->input('find') . '%');
        }

        // price range is a multi-select field so it's queried as:
        // WHERE (CONITION_1 OR CONDITION_2)
        $price = $request->input('price');
        if (is_array($price) && count($price) > 0) {
            // we get just a price range key from the form, and need to convert
            // it to a price range it refers to
            $ranges = Product::getPriceRanges();
            $conditions = [];
            $params = [];
            foreach ($price as $p) {
                if (isset($ranges[$p])) {
                    $range = $ranges[$p];
                    // if both min and max values are set use "BETWEEN min AND max"
                    if ($range['min'] !== null && $range['max'] !== null) {
                        $conditions[] = '(price BETWEEN ? AND ?)';
                        $params[] = $range['min'];
                        $params[] = $range['max'];
                    }

                    // no min value given in the range, so use just "<= max"
                    if ($range['min'] === null && $range['max'] !== null) {
                        $conditions[] = '(price <= ?)';
                        $params[] = $range['max'];
                    }

                    // no max value given in the range, so use just ">= min"
                    if ($range['min'] !== null && $range['max'] === null) {
                        $conditions[] = '(price >= ?)';
                        $params[] = $range['min'];
                    }
                }
            }
            $productsQuery->whereRaw('(' . implode(' OR ', $conditions) . ')', $params);
        }
       
        if ($this->isWishlist) {
            $user = \Auth::user();
            if (empty($user)) {
                return redirect('noaccount');
            }
            $productsQuery->join('user_wishlists', 'products.id', '=', 'user_wishlists.product_id');
            $productsQuery->where('user_wishlists.user_id', '=', $user->id);
            $productsQuery->select('products.*');
        }

        if ($this->onUserWishlist) {
            $productsQuery->join('user_wishlists', 'products.id', '=', 'user_wishlists.product_id');
            $productsQuery->where('user_wishlists.user_id', '=', $this->onUserWishlist);
            $productsQuery->select('products.*');
        }

        // total product count for current filters
        $count          = $productsQuery->count();
        // total number of pages for the list
        $pageNumber     = 1 + (int)($count / $productsPerPage);
        $products       = $productsQuery
            ->take($productsPerPage)
            ->skip($offset)
            ->with(['attributes', 'wishlist'])
            ->get();

        if ($this->isWishlist && $count == 0) {
            return view('emptywishlist');
        }
        return view('welcome', [
            'products'      => $products,
            'count'         => $count,
            'filters'       => $request->all(),
            'showing'       => min($count, $productsPerPage, count($products)),
            'howMany'       => $productsPerPage,
            'pageNumber'    => $pageNumber,
            'page'          => min($page, $pageNumber),
            'isWishlist'    => $this->isWishlist,
            'headerText'    => $headerText,
            'showFilters'   => $showFilters,
        ]);
    }

    public function product(Request $request, $id)
    {
        $product = Product::where([
            'id' => $id,
        ])->take(1)->first();
        
        return view('product', [
            'product' => $product
        ]);
    }

    public function view(Request $request, $id)
    {
        $product = Product::where([
            'id' => $id,
        ])->take(1)->first();
        
        return view('view', [
            'product' => $product,
        ]);
    }
}
