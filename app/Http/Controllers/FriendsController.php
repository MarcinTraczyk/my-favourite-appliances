<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\User;
use App\UserFriend;

class FriendsController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        $user = \Auth::user();
        $friends = [];
        $invites = [];
        $pending = [];
        if (!empty($user)) {
            $friends = $user->friends;
            $invites = $user->invites;
            $pending = $user->pendingInvites;
        }
        return view('friends', [
            'friends' => $friends,
            'invites' => $invites,
            'pending' => $pending,
        ]);
    }

    public function search(Request $request)
    {
        $userList = [];
        try {
            $user = \Auth::user();
            $search = $request->input('search');
            if ($user && !empty($search)) {
                $userList = User::select('users.*')
                    ->where('users.name', 'like', "%" . $search . "%")
                    ->leftJoin('user_friends AS uf', function($join) use($user){
                        $join->on('uf.friend_id', '=', 'users.id');
                        $join->where('uf.user_id', '=', $user->id);
                    })
                    ->leftJoin('user_friends AS uf2', function($join) use($user){
                       $join->on('uf2.user_id', '=', 'users.id');
                       $join->where('uf2.friend_id', '=', $user->id);
                    })
                    ->whereNull('uf.id')
                    ->whereNull('uf2.id')
                    ->take(10)
                    ->get()
                    ->toArray();
            }
        } catch(\Exception $e) {
            //
        }

        $returnHtml = "";
        if (empty($userList)) {
            $returnHtml .= '<li class="list-group-item">Sorry, no users found <i class="far fa-surprise"></i></li>';
        } else {
            foreach ($userList as $u) {
                $name = $u['name'];
                $id = $u['id'];
                $returnHtml .= "<li class='request-friend list-group-item' data-id='$id'><i class='fas fa-user-plus'></i>&nbsp;$name</li>";
            }
        }

        return response()->json([
            'status'    => 'ok',
            'html'      => $returnHtml,
        ]);
    }

    public function add(Request $request, $id)
    {
        try {
            $user       = $request->user();
            $friend     = intval($id);
            $status     = 0;
            $message    = 'Fail';
            $isFriend   = UserFriend::userIsFriend($friend);
            if ($isFriend) {
                $message = 'User already a friend';
            } else if ($user && $friend) {
                $record = new UserFriend([
                    'friend_id'     => $friend,
                    'user_id'       => $user->id,
                    'status'        => 'invite',
                ]);
                
                $status = $record->save();
                if ($status) {
                    $message = 'Success';
                } else {
                    $message = 'Error saving record';
                }
            }

        } catch (\Exception $e) {
            $message = 'EPR';
        }

        return response()->json([
            'status'    => $status,
            'message'   => $message,
        ]);
    }

    public function accept(Request $request, $id)
    {
        $id = intval($id);
        $user = $request->user();
        if (!empty($user) && $id) {
            \DB::beginTransaction();
            try {
                $inviteRecord = UserFriend::where('user_id', '=', $id)
                    ->where('friend_id', '=', $user->id)
                    ->take(1)
                    ->first();
                $inviteRecord->status = 'friend';
                $inviteRecord->save();

                $newRecord = new UserFriend([
                    'friend_id' => $id,
                    'user_id'   => $user->id,
                    'status'    => 'friend'
                ]);
                $newRecord->save();
                \DB::commit();
            } catch(\Exception $e) {
                \DB::rollback();
            }
        }

        return redirect('friends');
    }
}
