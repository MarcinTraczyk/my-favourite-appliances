<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\UserShare;

class SharesController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function link(Request $request)
    {
        try {
            $user       = $request->user();
            $link       = $request->input('link');
            $users      = $request->input('users');
            $type       = $request->input('type');
            $status     = 0;
            $message    = 'Fail';

            \DB::beginTransaction();
            try {
                foreach ($users as $userId) {
                    $record = new UserShare([
                        'sender_id'     => \Auth::user()->id,
                        'recipient_id'  => $userId,
                        'link'          => $link,
                        'type'          => $type,
                        'seen'          => false,
                    ]);
                    $record->save();
                }
                \DB::commit();
            } catch(\Exception $e) {
                \DB::rollback();
            }
            $status = 1;
            $message = 'success';
        } catch (\Exception $e) {
            throw $e;
            $message = 'EPR';
        }
        return response()->json([
            'status'    => $status,
            'message'   => $message,
        ]);
    }

    public function list(Request $request)
    {
        $user = $request->user();
        $shares = [];
        if (!empty($user)) {
            $shares = $user->shares;
        }

        return view('shares', ['shares' => $shares]);
    }

    public function seen(Request $request, $id)
    {
        try {
            $id = intval($id);
            if ($id > 0) {
                $record = UserShare::where('id', '=', $id)->first();
                $record->seen = true;
                $record->save();
            }
        } catch (\Exception $e) {
            //
        }
    }
}