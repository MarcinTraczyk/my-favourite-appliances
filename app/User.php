<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function wishlist()
    {
        return $this->hasMany('App\UserWishlist');
    }

    public function friends()
    {
        return $this->hasMany('App\UserFriend')
            ->where('status', '=', 'friend')
            ->with('friendUser');
    }

    public function shares()
    {
        return $this->hasMany('App\UserShare', 'recipient_id', 'id');
    }

    public function invites()
    {
        return $this->hasMany('App\UserFriend', 'friend_id', 'id')
            ->where('status', '=', 'invite')
            ->orderBy('created_at', 'desc')
            ->with('user');
    }

    public function pendingInvites()
    {
        return $this->hasMany('App\UserFriend')
            ->where('status', '=', 'invite')
            ->with('friendUser');
    }

    public function getWishlist() {
        return $this->getRelationValue('wishlist')->keyBy('product_id');
    }

    public function getFriendlist() {
        return $this->getRelationValue('friends')->keyBy('friend_id');
    }

    public function getOrCreateWishlistHash()
    {
        if (empty($this->wishlist_hash)) {
            $this->wishlist_hash = md5('super_fancy_s@lt' . $this->email . $this->name);
            $this->save();
        }
        return $this->wishlist_hash;
    }
}
