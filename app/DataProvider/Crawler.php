<?php 

namespace App\DataProvider;

class Crawler {
    // target url to crawl
    protected $site = null;
    // this will be used as get parameter to go over all product pages
    protected $getParameter = 'page';
    // batch size for parallel curl requests
    protected $batchSize = 5;
    protected $maxBatches = 10;
    // final product data array
    protected $data = [];
    // product type identifier
    protected $category = null;
    // ---------------------- variables for logging purposes
    protected $batchesFetched = 0;
    protected $pagesFound = 0;
    protected $start = null;
    protected $duration = null;
    protected $requestsSent = 0;
    // -----------------------------------------------------
    protected static $isSetup = false;

    protected static function setup()
    {
        if (!self::$isSetup) {
            define('MAX_FILE_SIZE', 6000000);
            self::$isSetup = true;
        }
    }

    function __construct($site, $category, $batchSize=5)
    {
        $this->site = $site;
        $this->batchSize = $batchSize;
        $this->category = $category;
        self::setup();
    }

    public function getCurrentUrl($iterator)
    {
        return $this->site . '?' . $this->getParameter . '=' . $iterator;
    }

    /**
     * Executes curls requests to all product pages and populates $this->data array
     */
    public function fetchAllPages()
    {
        // crawler was not initialized properly
        if (is_null($this->site)) {
            return false;
        }

        $this->start = time();
        $start = microtime(true);

        $runNextBatch = true;
        $batchIterator = 0;

        // curl options to be added to each request
        $options = array(
            CURLOPT_CUSTOMREQUEST  =>"GET",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_CONNECTTIMEOUT => 30,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_MAXREDIRS      => 10,
        );

        // fetching one page at a time is slow, better to grab them in parallel
        // this will execute batches of GET requests of the size of $this->batchSize
        // until data is returned (with an extra sanity check, just in case)
        while ($runNextBatch && $batchIterator < $this->maxBatches) {
            $curlArray = [];
            // mulit-handle for parallel gets
            $master = curl_multi_init();

            // create a GET request to 
            // (assuming $this->site = "www.example.com" and $this->getParameter='page'):
            // www.example.com?page=$index
            for ($iterator = 1; $iterator < $this->batchSize + 1; $iterator++) {
                $index = $iterator + $this->batchSize * $batchIterator;
                $ch = curl_init($this->getCurrentUrl($index));
                curl_setopt_array($ch, $options);
                curl_multi_add_handle($master, $ch);
                $curlArray[] = $ch;
                $this->requestsSent++;
            }

            // execute all requests
            do {
                curl_multi_exec($master,$running);
            } while($running > 0);

            $results = [];
            foreach ($curlArray as $ch) {
                $results[] = curl_multi_getcontent($ch);
            }

            // clean all handlers
            foreach ($curlArray as $ch) {
                curl_multi_remove_handle($master, $ch);
            }
            curl_multi_close($master);
            
            // parse results HTML into products array
            foreach ($results as $content) {
                $data = $this->parseHtmlIntoProductArray($content);
                // nothing new got back, do not execute next batch
                if (count($data) == 0) {
                    $runNextBatch = false;
                } else {
                    $this->data += $data;
                    $this->pagesFound++;
                }
            }
            $batchIterator++;
        }
        
        $this->batchesFetched = $batchIterator;
        $end = microtime(true);
        $this->duration = $end - $start;
        return true;
    }

    /**
     * Parse page content (HTML) into an array with product details
     */
    protected function parseHtmlIntoProductArray($content)
    {
        $html = \HTMLDomParser::str_get_html($content);
        if (!$html) {
            return [];
        }
        $products = $html->find('body .search-results-product');

        $data = [];
        if (is_array($products) && count($products) > 0) {
            foreach ($products as $product) {
                $id = null;
                $name = null;
                $rrp = null;
                $price = null;
                $image = null;
                $attributes = [];
                // I assume here that the numeric part of the URL on the first anchor tag
                // corresponds to a unique product ID. This assumption holds for the 
                // current state of www.appliancesdelivered.ie
                $anchors = $product->find('a');
                if (count($anchors) > 0) {
                    $href = explode('/', $anchors[0]->href);
                    $id = end($href);
                }

                // get product name from the header tag
                $h4 = $product->find('h4');
                if (count($anchors) > 0) {
                    $name = $h4[0]->plaintext;
                }

                // get RRP from .price-value element
                $el = $product->find('.price-value');
                if (count($el) > 0) {
                    $rrp = $el[0]->plaintext;
                }

                // get price from the h3 title
                $el = $product->find('h3.section-title');
                if (count($el) > 0) {
                    $price = $el[0]->plaintext;
                }

                // get additional atrtibutes from the <ul>
                $ul = $product->find('ul');
                if (count($ul) > 0) {
                    $lis = $ul[0]->find('li');
                    if (count($lis) > 0) {
                        foreach ($lis as $li) {
                            $attributes[] = $li->plaintext;
                        }
                    }
                }

                // get product image url
                $img = $product->find('img');
                if (count($img) > 0) {
                    $image = $img[0]->src;
                }

                if (is_numeric($id)) {
                    $data[$id] = [
                        'name'          => trim($name),
                        'rrp'           => $rrp,
                        'price'         => $price,
                        'attributes'    => $attributes,
                        'id'            => $id,
                        'category'      => $this->category,
                        'image_url'     => $image,
                    ];
                }
            }
        }

        return $data;
    }

    public function getResults()
    {
        return [
            'duration'      => $this->duration,
            'start'         => $this->start,
            'pages'         => $this->pagesFound,
            'batches'       => $this->batchesFetched,
            'requests'      => $this->requestsSent,
            'data'          => $this->data,
        ];
    }
}