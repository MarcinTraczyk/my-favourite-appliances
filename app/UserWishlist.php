<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserWishlist extends Model
{
    protected $fillable = ['user_id', 'product_id'];

    public static function productOnWishlist($productId)
    {
        $user = \Auth::user();
        if (!empty($user)) {
            $list = $user->getWishlist()->toArray();
            return in_array($productId, array_keys($list));
        }
        
        return false;
    }
}
