<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->integer('id');
            $table->string('name')->charset('utf8')->collation('utf8_unicode_ci');
            $table->string('category')->charset('utf8')->collation('utf8_unicode_ci');
            $table->string('image_url', 1023)->charset('utf8')->collation('utf8_unicode_ci');
            $table->float('price', 8, 2);
            $table->float('rrp', 8, 2)->nullable();
            $table->timestampsTz();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
