<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // ----------------- create a bunch of test users
        $marcin = new App\User([
            'name'      => 'Marcin',
            'email'     => 'marcin@domain.com',
            'password'  => bcrypt('password'),
        ]);
        $marcin->save();

        $admin = new App\User([
            'name'      => 'Admin',
            'email'     => 'admin@here.com',
            'password'  => bcrypt('password'),
        ]);
        $admin->save();

        $johnDoe = new App\User([
            'name'      => 'John Doe',
            'email'     => 'john@doe.com',
            'password'  => bcrypt('password'),
        ]);
        $johnDoe->save();

        $eve = new App\User([
            'name'      => 'Eve Adams',
            'email'     => 'e.adams@internet.com',
            'password'  => bcrypt('password'),
        ]);
        $eve->save();

        // ----------------- make them friends
        $friend = new App\UserFriend([
            'user_id'       => $marcin->id,
            'friend_id'     => $admin->id,
            'status'        => 'friend'
        ]);
        $friend->save();

        $friend = new App\UserFriend([
            'user_id'       => $admin->id,
            'friend_id'     => $marcin->id,
            'status'        => 'friend'
        ]);
        $friend->save();

        $friend = new App\UserFriend([
            'user_id'       => $admin->id,
            'friend_id'     => $johnDoe->id,
            'status'        => 'invite'
        ]);
        $friend->save();

        $friend = new App\UserFriend([
            'user_id'       => $eve->id,
            'friend_id'     => $admin->id,
            'status'        => 'invite'
        ]);
        $friend->save();

        // ----------------- Marcin's wishlist 
        $products = ['4534', '4533', '4531', '4515', '1472'];
        foreach ($products as $id) {
            $record = new App\UserWishlist([
                'product_id'    => $id,
                'user_id'       => $marcin->id,
            ]);
            $record->save();
        }

        // ----------------- Marcin shared his wishlist with Admin
        $share = new App\UserShare([
            'sender_id'     => $marcin->id,
            'recipient_id'  => $admin->id,
            'type'          => 'wishlist',
            'link'          => '/share/' . $marcin->getOrCreateWishlistHash(),
            'seen'          => false,
        ]);
        $share->save();

        // ----------------- Marcin is a spammer, so he shared other stuff too
        $products = ['4534', '4533'];
        foreach ($products as $id) {
            $share = new App\UserShare([
                'sender_id'     => $marcin->id,
                'recipient_id'  => $admin->id,
                'type'          => 'product',
                'link'          => '/view/' . $id,
                'seen'          => false,
            ]);
            $share->save();
        }
    }
}
