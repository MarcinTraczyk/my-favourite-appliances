@if (!empty($product))
<div class="col-12 product-container row">
    @auth
        <div class="col-12 row buffer-bottom-small">
            <span class="fav-number">
                <button type="button" class="btn btn-outline-primary btn-sm {{ 
                    $product->onWishlist()
                    ? 'btn-wishlist'
                    : 'btn-like'
                }}" data-product="{{ $product->id }}">
                    {!!
                        $product->onWishlist()
                        ? '<i class="fas fa-star"></i>&nbsp;On my wishlist'
                        : '<i class="far fa-star"></i>&nbsp;I like it!'
                    !!}
                </button>
                <i class="fas fa-star">&nbsp;{{ count($product->wishlist) }}</i>
                <div class="fav-number-tooltip alert alert-info" style="z-index: 999;">
                    {{ count($product->wishlist) }} people have it on their wishlist
                </div>
            </span>
        </div>
    @endauth
    <div class="row col-12">
        <div class="col-12 col-sm-6 col-md-4">
            <img src="{{ $product->getImageUrl() }}"/>
        </div>
        <div class="col-12 col-sm-6 col-md-8 row">
            <h1 class="product-title font-medium">
                <a href="{{ route('view', $product->id) }}">{{ html_entity_decode($product->name) }}</a>
            </h1>
            <div class="col-12 row product-description">
                <div class="col-12 col-md-6 order-first order-md-last">
                    <div class="product-price">{{ $product->price }}</div>
                    @if ($product->rrp > 0)
                        <div class="font-medium">RRP:
                            <span class="product-rrp">{{ $product->rrp }}</span>
                        </div>
                        <div class="font-medium">Save:
                            <span class="product-rrp-percentage">{{ $product->getDiscountPercentage() }}</span>
                        </div>
                    @endif
                </div>
                <div class="col-12 col-md-6 order-last order-md-first">
                    <ul>
                        @foreach ($product->attributes as $attr)
                                <li>{{ html_entity_decode($attr->description) }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    @auth
        <div class="row product-share-row col-12">
            <div class="btn-right buffer-top-small">
                <span>Share with your friends:</span>
                <select class="selectpicker" multiple data-live-search="true">
                    @foreach (\Auth::user()->friends as $friend)
                        <option value="{{ $friend->friend_id }}">{{ $friend->friendUser->name }}</option>
                    @endforeach
                </select>
                <button type="button" class="btn btn-light share-product-link">Share</button>
                <span style="margin: 0px 10px;">or</span>
                <span class="copy-product-container">
                    <button type="button" class="btn btn-light copy-product-link">Copy link</button>
                </span>
            </div>
        </div>
    @endauth
</div>
@endif
