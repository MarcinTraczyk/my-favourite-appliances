@extends('layouts.app')

@section('content')
<div class="content container-fluid" id="main-container">
    <div class="row title-row">
        <h1>{{ $headerText }}</h1>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="float-right"> 
                <span>Showing <b>{{ $showing }}</b> </span>
                <span>out of <b>{{ $count }}</b> products, sorted by:</span>
                <select class="form-control inline-select" id="sort-by">
                    <option value="0" 
                        {{ isset($filters['order_by']) 
                            && $filters['order_by'] == 0
                            ? 'selected'
                            : ''
                        }}
                        >Name (alphabetical)</option>
                    <option value="1"
                        {{ isset($filters['order_by']) 
                            && $filters['order_by'] == 1
                            ? 'selected'
                            : ''
                        }}
                        >Name (reverse alphabetical)</option>
                    <option value="2"
                        {{ isset($filters['order_by']) 
                            && $filters['order_by'] == 2
                            ? 'selected'
                            : ''
                        }}
                        >Price (high to low)</option>
                    <option value="3"
                        {{ isset($filters['order_by']) 
                            && $filters['order_by'] == 3
                            ? 'selected'
                            : ''
                        }}
                        >Price (low to high)</option>
                </select>
                <span>with:</span>
                <select class="form-control inline-select" id="how-many">
                    <option value="10" 
                        {{ isset($filters['how_many']) 
                            && $filters['how_many'] == 10
                            ? 'selected'
                            : ''
                        }}
                        >10</option>
                    <option value="20"
                        {{ isset($filters['how_many']) 
                            && $filters['how_many'] == 20
                            ? 'selected'
                            : ''
                        }}
                        >20</option>
                    <option value="50"
                        {{ isset($filters['how_many']) 
                            && $filters['how_many'] == 50
                            ? 'selected'
                            : ''
                        }}
                        >50</option>
                </select>
                <span> results per page</span>
            </div>
        </div>
    </div>

    @if ($isWishlist)
        <div class="row buffer-bottom-small">
            <div class="col-12 filter-toggle-row" id="share-toggle" data-toggle="collapse" 
                href="#share-container" aria-expanded="true" aria-controls="share-container">
                <span class='filter-header'>Share&nbsp;<i class="fas fa-caret-up  "></i></span>
            </div>

            <div class="col-12 collapse show" id="share-container">
                <div>This is a link to your wishlist that you can share with your friends on social media or via messaging apps:</div>
                <a href="{{ route('sharewishlist', ['hash' => \Auth::user()->getOrCreateWishlistHash()]) }}" 
                    target="_blank" id="wishlist-link-anchor">
                    {{ route('sharewishlist', ['hash' => \Auth::user()->getOrCreateWishlistHash()]) }}
                </a>
                <button type="button" class="btn btn-light" id="copy-wishlist-link">Copy link</button>

                <div class="buffer-top-small">
                    <div>Share with your friends here:</div>
                    <select class="selectpicker" multiple data-live-search="true" id="share-with-users">
                        @foreach (\Auth::user()->friends as $friend)
                            <option value="{{ $friend->friend_id }}">{{ $friend->friendUser->name }}</option>
                        @endforeach
                    </select>
                    <button type="button" class="btn btn-light" id="share-wishlist-link">Share</button>
                </div>
            </div>
        </div>
    @endif

    @if ($showFilters)
    <div id="filter-bar" class="buffer-bottom-small row">
        <div class="col-12 filter-toggle-row" id="filter-toggle" data-toggle="collapse" 
            href="#filter-container" aria-expanded="true" aria-controls="filter-container">
            <span class='filter-header'>Filters&nbsp;<i class="fas fa-caret-up"></i></span>
        </div>

        <div class="col-12 collapse show" id="filter-container">
            <form class="row" method="get">
                <div class="col-12 col-sm-6 filter-column order-2 order-sm-1 buffer-bottom-small">
                    <div class="form-group">
                        <label for="category">Categories:</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="dishwasher" name="category[]" 
                                    {{ isset($filters['category']) 
                                        && in_array('dishwasher', $filters['category']) 
                                        ? 'checked' : '' 
                                    }}>&nbsp;Dishwashers
                                </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="small appliance" name="category[]"
                                {{ isset($filters['category']) 
                                    && in_array('small appliance', $filters['category']) 
                                    ? 'checked' : '' 
                                }}>&nbsp;Small Appliances
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="product-search">Search:</label>
                        <input class="form-control" type="search" name="find" 
                            placeholder="Search for products" id="product-search"
                            {{ isset($filters['find']) 
                                ? 'value=' . $filters['find']
                                : '' 
                            }}
                        >
                    </div>
                </div>
                <div class="col-12 col-sm-6 filter-column order-1 order-sm-2 buffer-bottom-small">
                    <div class="form-group checkbox-list">
                        <label for="price">Price range:</label>
                        @foreach (\App\Product::getPriceRanges() as $key => $range)
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="{{ $key }}" name="price[]"
                                {{ isset($filters['price']) 
                                    && in_array($key, $filters['price']) 
                                    ? 'checked' : '' 
                                }}
                                >&nbsp;{{ $range['display'] }}
                            </label>
                        </div>
                        @endforeach
                    </div>
                </div>
                
                <div class="col-12 col-sm-6 order-3 buffer-bottom-small">
                    <button type="button" class="btn btn-block btn-orange" onclick="javascript:window.location.href='/'">
                        <i class="fas fa-times"></i>&nbsp;Clear Filters
                    </button>
                </div>
                <div class="col-12 col-sm-6 order-4 buffer-bottom-small">
                    <button type="submit" class="btn btn-block btn-success">
                        <i class="fab fa-searchengin"></i>&nbsp;Search
                    </button>
                </div>
            </form>
        </div>
    </div>
    @endif

    <div class="row buffer-top-small" id="product-list">
        @foreach ($products as $product)
            @include('product', ['product' => $product])
        @endforeach
    </div>

    <div class="row" id="page-select" data-page="{{ $page }}" data-page-number="{{ $pageNumber }}">
        <div class="col-2">
            <button type="button" class="btn btn-block btn-primary" id="btn-first"
                {{ $page == 1 ? 'disabled' : '' }}
            >First</button>
        </div>
        <div class="col-2">
            <button type="button" class="btn btn-block btn-primary" id="btn-prev"
                {{ $page == 1 ? 'disabled' : '' }}
            >Prev</button>
        </div>
        <div class="col-4">
            <button type="button" class="btn btn-block" disabled id ="btn-page-counter">Page {{ $page }} / {{ $pageNumber }}</button>
        </div>
        <div class="col-2">
            <button type="button" class="btn btn-block btn-primary" id="btn-next"
                {{ $page == $pageNumber ? 'disabled' : '' }}
            >Next</button>
        </div>
        <div class="col-2">
            <button type="button" class="btn btn-block btn-primary" id="btn-last"
                {{ $page == $pageNumber ? 'disabled' : '' }}
            >Last</button>
        </div>

        <div class="col-12">
            <button type="button" class="btn btn-block btn-primary" id="btn-top">Back to top</button>
        </div>
    </div>
</div>
@endsection