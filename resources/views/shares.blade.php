@extends('layouts.app')

@section('content')
<div class="content container-fluid friends-container" id="main-container">
<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Sender</th>
            <th scope="col">Type</th>
            <th scope="col">Link</th>
            <th scope="col">Sent On</th>
            <th scope="col">Seen?</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($shares as $key => $share)
            <tr>
                <td>{{ $key + 1 }}</td>
                <td>{{ $share->sender->name }}</td>
                <td>{{ $share->type }}</td>
                <td><a data-id="{{ $share->id }}" class="shared-link-visit" href="{{ $share->link }}">(click to visit)</a></td>
                <td>{{ $share->created_at }}</td>
                <td>{{ $share->seen ? 'Yes' : 'No '}} </td>
            </tr>
        @endforeach
    </tbody>
</div>
@endsection