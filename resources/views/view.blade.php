@extends('layouts.app')

@section('content')
<div class="content container-fluid" id="main-container">
    <div class="row" id="product-list">
        @include('product', ['product' => $product])
    </div>
</div>
@endsection