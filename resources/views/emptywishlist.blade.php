@extends('layouts.app')

@section('content')

<div class="content container-fluid" id="main-container">
    <div id="message-container">
        <div>
            You don't have any products on your wishlist yet
        </div>
        <div>
            <a href="{{ route('main') }}">Browse the product list</a> and add items to your wishlist!
        </div>
    </div>
</div>
@endsection