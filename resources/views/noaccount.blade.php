@extends('layouts.app')

@section('content')

<div class="content container-fluid" id="main-container">
    <div id="message-container">
        <div>
            You need to <a href="{{ route('login') }}">log in</a> to view this part of the website
        </div>

        <div>
            Don't have an account? <a href="{{ route('register') }}">Create one now!</a>
        </div>
    </div>
</div>
@endsection