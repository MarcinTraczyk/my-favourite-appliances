@extends('layouts.app')

@section('content')

<div class="content container-fluid" id="main-container">
    <div id="message-container">
        <div>
            Sorry we didn't find what you were looking for :(
        </div>
    </div>
</div>
@endsection