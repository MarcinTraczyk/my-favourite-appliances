@extends('layouts.app')

@section('content')
<div class="content container-fluid friends-container" id="main-container">
    <div class="row col-12">
        <h2>My friends ({{ count($friends) }})</h2>
        <div class='col-12'>
            @foreach ($friends as $friend)
                <div class="alert alert-info friend-item">
                    <a href="{{ route('sharewishlist', ['hash' => $friend->friendUser->getOrCreateWishlistHash()]) }}">
                    {{ $friend->friendUser->name }}
                    </a>
                </div>
            @endforeach
        </div>
    </div>
    <div class="row col-12">
        <h2>Invitations ({{ count($invites) + count($pending) }})</h2>
        <div class='col-12'>
            @foreach ($invites as $invite)
                <div class="alert alert-success friend-item">
                    {{ $invite->user->name }}
                    <span style="float: right">
                        <a href=" {{ route('friendaccept', ['id' => $invite->user->id]) }}">(accept invitation)</a>
                    </span>
                </div>
            @endforeach

            @foreach ($pending as $invite)
                <div class="alert alert-warning friend-item">{{ $invite->friendUser->name }} <span style="float: right;">sent on: {{ $invite->sentOn() }}</span></div>
            @endforeach
        </div>
    </div>
    <div class="row col-12">
        <div class="row buffer-top-small col-12">
            <div class="form-group col-12">
                <label for="friend-search">Add a friend</label>
                    <input class="form-control" type="search" name="friend-search" 
                        placeholder="Search your friends" id="friend-search">
                </div>
            </div>
        </div>

        <div class="col-12 friend-search-results" id="friend-search-results">
            <ul class="list-group">
            </ul>
        </div>
    </div>
</div>
@endsection