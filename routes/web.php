<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\DataProvider\Crawler;
use App\Product;
use Illuminate\Http\Request;

// product list
Route::get('/', 'Search@search')->name('main');

// your wishlist
Route::get('/wishlist', 'Search@wishlist')->name('wishlist');
// share a wishlist
Route::get('/share/{hash}', 'Search@share')->name('sharewishlist');
// view single product
Route::get('/product/{id}', 'Search@product')->name('product');
// view user's wishlist
Route::get('/view/{id}', 'Search@view')->name('view');

// share a link with another user
Route::post('/share/link', 'SharesController@link')->name('sharelink');
// list of received shares (wishlists or products)
Route::get('/shares/list', 'SharesController@list')->name('sharelist');
// mark a share as "seen"
Route::get('/shares/seen/{id}', 'SharesController@seen');


// add product to your wishlist
Route::get('/like', 'Wishlist@add');
// remove product from your wishlist
Route::get('/unlike', 'Wishlist@remove');

// prompt to log in or register
Route::get('/noaccount', function(){
    return view('noaccount');
});
// couldn't find a resource page
Route::get('/noresource', function(){
    return view('noresource');
});

// list of friends
Route::get('/friends', 'FriendsController@list')->name('friends');
// search for user based on name
Route::post('/friends/search', 'FriendsController@search')->name('friendseach');
// send a friend request
Route::get('/friends/add/{id}', 'FriendsController@add')->name('friendseach');
// accept a friend request
Route::get('/friends/accept/{id}', 'FriendsController@accept')->name('friendaccept');

// Auth module views
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
